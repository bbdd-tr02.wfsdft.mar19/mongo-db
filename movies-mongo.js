db.movies.insertMany ([
    {
        title : 'Fight Club',
        writer : 'Chuck Palahniuk',
        year : 1999,
        actors : [
        'Brad Pitt',
        'Edward Norton']
    },
    {
        title : 'Pulp Fiction',
        writer : 'Quentin Tarantino',
        year : 1994,
        actors : [
        'John Travolta',
        'Uma Thurman'
        ]
    },
    {
        title : 'Inglorious Basterds',
        writer : 'Quentin Tarantino',
        year : 2009,
        actors : [
        'Brad Pitt',
        'Diane Kruger',
        'Eli Roth'
        ]
    },
    {
        title : 'The Hobbit: An Unexpected Journey',
        writer : 'J.R.R. Tolkein',
        year : 2012,
        franchise : 'The Hobbit'
    },
    {
        title : 'The Hobbit: The Desolation of Smaug',
        writer : 'J.R.R. Tolkein',
        year : 2013,
        franchise : 'The Hobbit'
    },
    {
        title : 'The Hobbit: The Battle of the Five Armies',
        writer : 'J.R.R. Tolkein',
        year : 2012,
        franchise : 'The Hobbit',
        synopsis : 'Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.'

    },
    {
        title : 'Pee Wee Herman\'s Big Adventure'
    },
    {
        title : 'Avatar'
    }

]);


db.movies.find({writer: 'Quentin Tarantino'},{_id:0, title:1});

db.movies.find({actors: {$in: ['Brad Pitt']}},{_id:0, title:1});

db.movies.find({franchise: 'The Hobbit'},{_id:0, title:1});

db.movies.find({year: { $gt :  1989, $lt : 2000}},{_id:0, title:1});

db.movies.find({year: { $gt :  1999, $lt : 2011}},{_id:0, title:1});

db.movies.updateOne({title: 'The Hobbit: An Unexpected Journey'},{ $set: {synopsis:'A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home -and the gold within it -from the dragon Smaug.'} });

db.movies.updateOne({title: 'The Hobbit: The Desolation of Smaug'},{ $set: {synopsis:'The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.'} });

db.movies.updateOne({title: 'Pulp Fiction'},{ $set: {"actors$":'Samuel L. Jackson'} });


'John Travolta',
        'Uma Thurman'

db.movies.updateOne({title: 'Pulp Fiction'},{ $push: {actors:['John Travolta','Uma Thurman']} });

db.movies.insertOne({
    "title" : "Pulp Fiction",
	"writer" : "Quentin Tarantino",
	"year" : 1994,
	"actors" : [
		"John Travolta",
		"Uma Thurman"
	]
});

db.movies.updateOne({title: 'Pulp Fiction'},{ $push: {actors:'Samuel L. Jackson'} });


db.movies.find({title:/Hobbit/},{title:1, _id:0});

db.movies.find({synopsis:/Gandalf/},{title:1, _id:0});


db.movies.find({$and:[{synopsis:/Bilbo/},{synopsys: {$not: /Gandalf/}} ]});


/* ALPHA */
db.peliculas.find({cast.character: 'John Wick' });

db.peliculas.find( { "cast.character": "John Wick" } )