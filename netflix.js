var netflix = [
    {
    peliculas:
    [
    {
    id: 1,
    nombre: "Homeland", 
    tipo: "serie",
    año: 2011,
    genero: ['Dramas TV'],
    url_imagen: "img/homeland.jpg",
    temporadas: 6,
    capitulos: [
        {
            temporada: 1,
            nombre_capitulo: "Capitulo serie 1",
            audio: ["español","inglés"],
            subtitulos: ["español","inglés","alemán","francés"],
            url_imagen_cap: "img/cap1-homeland.jpg",
            sinopsis_capitulo: "Un marine prisionero de guerra en Afganistán regresa a casa convertido en héroe, pero una agente de la CIA sospecha que podría ser un agente enemigo.",
            duracion: 50
          },
          {
            temporada: 2,
            nombre_capitulo: "Capitulo serie 2",
            audio: ["español","inglés"],
            subtitulos: ["español","inglés","alemán","francés"],
            url_imagen_cap: "img/cap1-homeland.jpg",
            sinopsis_capitulo: "Esta es la sinopsis del capitulo 2"
            duracion: 48.5
          }
    ],
    resumen: "La analista de la CIA, Carrie Mathison, libra su propia batalla contra la enfermedad mental y algunos problemas familiares, mientras dirige la lucha contra el terrorismo.",
    puntuacion: 5,
    clasificacion_edad: 16,
    protagonistas: ['Claire Danes', 'Mandy Patinkin', 'Damian Lewis'],
    director: ['Howard Gordon', 'Alex Gansa']
    },
    {
    id: 2,
    nombre: "The Blacklist", 
    tipo: "serie",
    año: 2013,
    genero: ['Thrillers TV','Dramas TV','Series TV policiacas'],
    url_imagen: "img/blacklist.jpg",
    temporadas: 5,
    capitulos: [
       {
             temporada: 1,
             nombre_capitulo: "Capitulo 1 - The Blacklist",
             audio: ["español","inglés"],
            subtitulos: ["español","inglés"],
            url_imagen_cap: "img/cap1-blacklist.jpg",
             sinopsis_capitulo: "aqui es la sinopsis del capitulo 1 The Blacklist",
            duracion: 41
              },
              {
            temporada: 2,
            nombre_capitulo: "Capitulo 2 - The Blacklist",
            audio: ["español","inglés"],
            subtitulos: ["español","inglés"],
            url_imagen_cap: "img/cap2-blacklist.jpg",
            sinopsis_capitulo: "Esta es la sinopsis del capitulo 2 The Blacklist",
            duracion: 46,
              }
        ],
    resumen: "Un brillante fugitivo se entrega al FBI y se ofrece para capturar criminales, pero solo si forma equipo con Elizabeth Keen, una analista criminal con escasa experiencia.",
    puntuacion: 4.5,
    clasificacion_edad: 16,
    protagonistas: ['James Spader', 'Megan Boone', 'Diego Klattenhoff'],
    director: ['Jon Bokenkamp']
        },
    { 
      id: 3,
      nombre: "El Padrino", 
      tipo: "pelicula",
      año: 1971,
      genero: ['Mafia','Dramas TV'],
      url_imagen: "img/padrino-1.jpg",
      resumen: "El Padrino (título original en inglés: The Godfather1​) es una película estadounidense de 1972 dirigida por Francis Ford Coppola. El filme fue producido por Albert S. Ruddy, de la compañía Paramount Pictures. Está basada en la novela homónima (que a su vez está basada en la familia real de los Mortillaro de Sicilia), de Mario Puzo, quien adaptó el guión junto a Coppola y Robert Towne,",
      puntuacion: 4.5,
      clasificacion_edad: 16,
      protagonistas: ['Marlon Brando','Al Pacino','Robert Duvall','James Caan','Richard Castellano','Diane Keaton'],
      director: ['Francis Ford Coppola']
  }
    ]
]