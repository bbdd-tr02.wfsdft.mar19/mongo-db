db.usuario.insertOne ({
    _id: 'usr1',
    nombre: 'Carlitos',
    password: 'contlaseña de calitos',
    email: 'carlitos@segoviaatope.com',
    posts: [
        DBRef('posts','1'),
        DBRef('posts','4'),
        DBRef('posts','8'),
        DBRef('posts','9')
    ]  
})

db.posts.insertOne ({
    _id: ObjectId("usr1"),
    titulo: 'La vida de las pirañas',
    contenido: 'pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas pirañas',
    valoracion: [
        DBRef('valoracion', 'valoracion10')
    ],

    media: [
        {
            titulo: 'La vida de las pirañas',
            formato: 'jpg',
            url: 'gshjsagdjsagdja.jpg'
        },
        {
            titulo: 'pirañassss',
            formato: 'jpg',
            url: 'pirañassssss.jpg'
        }
    ],
    tags: [
        DBRef('tags','tag5'),
        DBRef('tags','tag6'),
        DBRef('tags','tag7'),
    ],
    comentarios: [
        DBRef('comentario','coment13'),
        DBRef('comentario','coment14'),
    ],


})
/*
db.media.insertOne ({
    _id: ObjectId("post100"),
    titulo: 'La vida de las pirañas',
    formato: 'jpg',
    url: 'gshjsagdjsagdja.jpg'
})

db.media.insertOne ({
    _id: ObjectId("post101"),
    titulo: 'La vida de las pirañas en video',
    formato: 'avi',
    url: 'gshjsagdjsagdja.avi'
})
*/
db.valoracion.insertOne({
    _id: ObjectId('valoracion1'),
    nombre: 'maria',
    valoracion: 5
})

db.tags.insertOne ({
    id: ObjectId("tag5"),
    nombre: 'piraña',
    url: '/piraña'
})

db.tags.insertOne ({
    id: ObjectId("tag6"),
    nombre: 'peces',
    url: '/peces'
})

db.comentarios.insertOne ({
    id: ObjectId("coment13"),
    texto: 'las pirañas son mis amigas',
    usuario: ObjectId("5")
})