db.restaurantes.find({},{restaurant_id:1, name:1, borough:1, cuisine:1, _id:0});

db.restaurantes.find({},{restaurant_id:1, name:1, borough:1, cuisine:1, _id:0}).count();

db.restaurantes.find({borough:'Bronx'},{name:1, _id:0})

// Restaurantes Bronx
db.restaurantes.find({borough:'Bronx'},{name:1, _id:0});
db.restaurantes.find({borough:'Bronx'},{name:1, _id:0}).count();

//5 primeros
db.restaurantes.find({borough:'Bronx'},{name:1, _id:0}).limit(5);

//2 siguientes y skip a los 5 primeros
db.restaurantes.find({borough:'Bronx'},{name:1, _id:0}).skip(5).limit(2);

//Puntuacion > 90
db.restaurantes.find( {'grades.score': { $gt:  90} } , {name:1, _id:0});

//Puntuacion entre 80 y 100
db.restaurantes.find( {'grades.score': { $gt :  80, $lt : 100} } , {name:1, _id:0});

// Latitud <

db.restaurantes.find( {'address.coord.1': { $lt:  -95.754168} } , {name:1, _id:0});


// Latitud < 70 y cocina no estadounidense

db.restaurantes.find( {'address.coord.1': { $lt:  -65.754168}, 'grades.score': { $gt:  70} , 'cuisine': { $ne:  "American "} } , {name:1, _id:0});

// Igual pero con latitud < 70

db.restaurantes.find( {'address.coord.0': { $lt:  -65.754168}, 'grades.score': { $gt:  70} , 'cuisine': { $ne:  "American "} } , {name:1, _id:0});

//Encontrarlos restaurantes que no preparan ninguna cocina "estadounidense" y que haya obtenido un punto de calificación "A" 
//que no pertenece al distrito de Brooklyn.El documento debe mostrarse de acuerdo con el tipo decocina en orden descendente.

db.restaurantes.find( { 'grades.grade': 'A' , 'cuisine': { $ne:  "American "}, borough: {$ne: 'Brooklyn'} } , {name:1, cuisine:1, _id:0}).sort({cuisine: -1});

//Encontrarel ID del restaurante, el nombre, el distrito yel tipo decocina para los restaurantes que contienen 'Wil' 
//como las tres primeras letras de su nombre.
db.restaurantes.find({name:/^Wil/}, {name:1,borough:1,_id:0 } );

//Encontrarel ID del restaurante, el nombre, el distrito yel tipo decocina de los restaurantes que contienen 'ces' como las tres últimas letras de su nombre.

db.restaurantes.find({name:/ces$/}, {name:1,borough:1, cuisine:1 } );

//Encontrarel ID del restaurante, el nombre, el distrito y el tipo decocina para los restaurantes que contienen 'Reg' como tres letras en algún lugar de su nombre
db.restaurantes.find({name:/Reg/}, {name:1,borough:1, cuisine:1 } );

//Encontrarlos restaurantes que pertenecen al Bronx de la ciudad y preparó un plato estadounidense o chino
db.restaurantes.find({borough:'Bronx', $or: [{cuisine: "American "} , {cuisine: "Chinese"}]}, {name:1,borough:1, cuisine:1 } );

//Encontrarel ID del restaurante, el nombre, el distrito y el tipo decocina de los restaurantes 
//que pertenecen al condado de Staten Island o Queens o Bronx or Brooklyn.
db.restaurantes.find({ $or: [{borough: "Bronx"} , {borough: "Staten Island"},{borough: "Queens"}, {borough: "Brooklyn"} ]}, {name:1,borough:1, cuisine:1 } );

//Encontrarel ID del restaurante, el nombre, el distrito y el tipo de cocina para los restaurantes que no pertenecen al distrito de Staten Island o Queens o Bronxor Brooklyn
db.restaurantes.find( {borough: {$nin: ["Bronx","Staten Island","Queens","Brooklyn"]} }, {name:1,borough:1, cuisine:1 } );

//Encontrarel ID del restaurante, el nombre, el distrito y el tipo de cocinapara los restaurantes que obtuvieron una puntuación que no supera el 10.
db.restaurantes.find({'grades.score': { $lt:  10}},{name:1,borough:1, cuisine:1 });

//Encontrarel ID del restaurante, el nombre, el distrito y el tipo decocina de los restaurantes que prepararon un plato, 
//excepto "Americano" y "Chinees" o el nombre del restaurante comienza con la letra "Wil"

db.restaurantes.find({name:/^Wil/,cuisine: {$nin: ["American ","Chinese"]}}, {name:1,borough:1, cuisine:1 } );

//Encontrarel ID del restaurante, el nombre y las calificaciones de esos restaurantes que obtuvieron una calificación de "A" y obtuvieron 11 puntos 
//en un ISODate "2014-08-11T00: 00: 00Z" entre muchas de las fechas de laencuesta"grades.date":ISODate("2014-08-11T00:00:00Z")

db.restaurantes.find({'grades.grade': 'A', 'grades.score': { $eq:  11}, 'grades.date': { $eq:  ISODate("2014-08-11T00:00:00Z")}}, {name:1,'grades.grade': 1,borough:1, cuisine:1 } );

//Encontrarel ID del restaurante, el nombre y las calificaciones de los restaurantes en los que el segundo elemento de la matriz de calificaciones contiene 
//una calificación de "A" y una puntuación de 9 en un ISODate "2014-08-11T00: 00: 00Z"
db.restaurantes.find({'grades.grade.1': 'A', 'grades.score.1':  9, 'grades.date': { $eq:  ISODate("2014-08-11T00:00:00Z")}}, {name:1,'grades.grade': 1,borough:1, cuisine:1 } );
