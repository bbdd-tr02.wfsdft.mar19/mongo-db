use alpha;

db.createCollection('peliculas');

db.peliculas.insertOne (
    {
    id: 10730,
    title: "King Kong",
    poster_path: "/qqj1fYVt0QGVBZD6HGmX8idnX1v.jpg",
    backdrop_path: "/ydiIbLKBH1bpDd9HJRKSa1RIkIF.jpg",
    genre_ids: [12,18],
    genres: [
        {
            "id": 12,
            "name": "Aventura"
        },
        {
            "id": 18,
            "name": "Drama"
        }
    ],
    runtime: 134,
    year: 1970,
    vote_average: 7,
    overview: "La \"Petrox Company\" envía una expedición, dirigida por Fred Wilson, a la Micronesia con la intención de encontrar petróleo. Con ellos va Jack Prescott, cuyo objetivo es encontrar un monstruo prehistórico. Durante la travesía, se encuentran un bote con una mujer que se ha salvado del naufragio del barco en que viajaba con un productor de cine. Cuando desembarcan, descubren una gran empalizada, y dentro observan que unos nativos colocan a una mujer nativa en una plataforma, mientras invocan a una misteriosa deidad animal.",

});

db.peliculas.insertOne (
    {
    id: 238,
    title: "El Padrino",
    poster_path: "/dfEQMuZMIcPgC7nt07D9uVQi7Tv.jpg",
    backdrop_path: "/6xKCYgH16UuwEGAyroLU6p8HLIn.jpg",
    genre_ids: [12,18],
    genres: [
        {
            "id": 18,
            "name": "Drama"
        },
        {
            "id": 80,
            "name": "Crimen"
        }
    ],
    runtime: 175,
    year: 1972,
    vote_average: 8.6,
    overview: "Don Vito Corleone, conocido dentro de los círculos del hampa como \"El Padrino\", es el patriarca de una de las cinco familias que ejercen el mando de la Cosa Nostra en Nueva York en los años 40. Don Corleone tiene cuatro hijos; una chica, Connie, y tres varones, Sonny, Michael y Freddie, al que envían exiliado a Las Vegas, dada su incapacidad para asumir puestos de mando en la \"Familia\". Cuando el Padrino reclina intervenir en el negocio de estupefacientes, empieza una cruenta lucha de violentos episodios entre las distintas familias del crimen organizado.",

});



db.creditos.insertOne(
    {
        id: 238,
        cast: [{
            name: "Marlon Brando",
            id: 3084},
            {
            name: "Al Paccino",
            id: 1158
            }
        ],
        crew: [{
            id: 1776,
            job: "Director",
            name: "Francis Ford Coppola"
        }
        ]
    }
);

db.MaestroPeliculas.insertOne(
    {
        Novedades: ['Array ids peliculas'],
        SeriesTV: ['Array ids peliculas'],
        Popular: ['Array ids peliculas'],
        Tendencias: ['Array ids peliculas'],
        Comedias: ['Array ids peliculas'],
        DramasTV: ['Array ids peliculas']
    }
);

db.alphaUsers.insertOne({
    id: 3001,
    email: 'elputoamo@alphastreaming.com',
    passwd: 'md5',
    dni: '000078953F',
    direccion: 'calle orense 69',
    ciudad: 'madrid',
    cp: 280001,
    comunidad: 'Madrid',
    misPeliculas: {
        Novedades: ['Array ids peliculas'],
        SeriesTV: ['Array ids peliculas'],
        Popular: ['Array ids peliculas'],
        Tendencias: ['Array ids peliculas'],
        Comedias: ['Array ids peliculas'],
        DramasTV: ['Array ids peliculas'],
        MiLista: ['Array ids peliculas'],
        SeguirViendo: ['Array ids peliculas'],
    },
});



https://api.themoviedb.org/3/movie/438650?api_key=b623b1e7ec090ee229dbf096d96c976c&language=es-ES

https://api.themoviedb.org/3/credit/438650?api_key=b623b1e7ec090ee229dbf096d96c976c

https://api.themoviedb.org/3/movie/438650/credits?api_key=b623b1e7ec090ee229dbf096d96c976c