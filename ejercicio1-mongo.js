use personas;

db.createCollection('alumnos');

db.alumnos.insertOne (
    {
    id: 1,
    nombre: 'mongo',
    apellidos: 'mongo',
    asignatura: 'mongo',
    fecha: new Date()
}
);


db.alumnos.insertMany (
[
    {
        id: 2,
        nombre: 'Pepe',
        apellidos: 'Pepito',
        asignatura: 'Base de datos',
        nacionalidad: 'ezpañol',
        fecha: new Date()
    },
    {
        id: 3,
        nombre: 'Pepe',
        apellidos: 'Otro pepe',
        asignatura: 'PHP a tope',
        fecha: new Date()
    },
    {
        id: 4,
        nombre: 'Pepe',
        apellidos: 'El de verdad',
        asignaturas: ['PHP','Javascript'],
        fecha: new Date()
    },
    {
        id: 5,
        nombre: 'Mery',
        apellidos: 'Mery',
        asignaturas: ['docker','compose'],
    },
    {
        id: 6,
        nombre: 'Maria',
        asignaturas: ['Mongo','docker'],
    },
    {
        id: 7,
        nombre: 'Maria',
        apellidos: 'Maria Maria',
        asignatura: 'Base de datos',
        nacionalidad: 'Española',
        fecha: new Date()
    },
    {
        id: 8,
        nombre: 'Carlos',
        apellidos: 'Jefe de Segovia',
        asignatura: 'Mongo',
        nacionalidad: 'Segoviano',
        fecha: new Date()
    },
    {
        id: 9,
        nombre: 'Carlitos',
        apellidos: 'El rey del vaper',
        asignatura: ['Angular','Vanilla JS'],
        nacionalidad: 'Segoviano',
        fecha: new Date()
    },
    {
        id: 10,
        nombre: 'Nombre',
        apellidos: 'Apellidos',
        asignatura: ['Angular','Vanilla JS'],
        nacionalidad: 'Spain',
        fecha: new Date()
    }
]);


db.alumnos.find().pretty();

db.alumnos.find({nombre: 'Pepe'}).pretty();
db.alumnos.find({nombre: 'Pepe'},{apellidos:1}).pretty();
db.alumnos.count();

db.alumnos.count({nombre: 'Pepe'});

db.alumnos.updateMany({nombre: 'Pepe'},{ $set: { "Edad" : 29 } });

db.alumnos.update({nombre: 'Carlitos'},{ $set: { "email" : "carlitos@segoviano.com" } });

db.alumnos.find({email: 'carlitos@segoviano.com'},{apellidos:1, _id:0}).pretty();

db.alumnos.deleteOne({nombre: 'mongo'});

db.alumnos.deleteMany({Edad: {$gt: 22}});

db.alumnos.update({apellidos: 'Jefe de Segovia'},{ $set: { "email" : "pepes@segoviano.com", Edad: 34 } });

db.alumnos.updateOne({apellidos: 'Jefe de Segovia'},{ $inc: {Edad:2} }); // incrementa 2

db.alumnos.updateOne({Edad: 36}, {$unset: {apellidos: ''}});

db.alumnos.replaceOne({nombre: 'Maria'}, {nombre: 'Maria 2', Edad: 25});

// Ejercicio 1c
db.alumnos.distinct('nombre');
db.alumnos.distinct('email');

db.alumnos.count();
db.alumnos.find().limit(3);
db.alumnos.find().skip(1);

db.alumnos.find().skip(db.alumnos.count() -5);

db.alumnos.find().sort({Edad: 1});

db.alumnos.find().sort({nombre: -1}); //ordenados nombres alf desc


// Ejercicio 1d

db.alumnos.findOne({Edad: {$gt: 18}},{_id: 0});

db.alumnos.findOne({nombre: 'Carlitos'},{_id: 0, apellidos: 1, email:1});

db.alumnos.findOneAndUpdate({Edad: {$gt: 18}},{ $inc: {Edad:2} },{sort: {Edad: 1}, returnNewDocument: true});

db.alumnos.updateMany({},{ $set: { "Puntuacion" : 15 } });

db.alumnos.updateOne({nombre: 'Maria 2'}, { $set: { "Puntuacion" : -5 } });

db.alumnos.updateOne({nombre: 'Carlos'}, { $set: { "Puntuacion" : -3 } });

db.alumnos.deleteMany({Puntuacion: {$lt: 0}});

db.alumnos.find({"email": {$exists: true}});


/* Ejercicio Ciudades */

db.ciudades.insertMany (
    [
{ "ciudad" : "Madrid", "habitantes" : 3233527, capital: "si"},
{ "ciudad" : "Barcelona", "habitantes" : 1620943 },
{"ciudad" : "Valencia", "habitantes" : 797028 },
{ "ciudad" : "Sevilla", "habitantes" : 702355 },
{"ciudad" : "Zaragoza", "habitantes" : 679624 },
    ]);


db.ciudades.find().sort({ciudad:1}).limit(3);

db.ciudades.find().sort({habitantes:-1});

db.ciudades.find({ciudad:/^M/});

db.ciudades.find().sort({ciudad:-1, habitantes:1});

db.ciudades.find({capital: "si"},{ciudad:1,_id:0});

db.ciudades.find({"capital": {$exists: true}});

db.ciudades.find({habitantes: {$gt: 1000000}},{ciudad:1,_id:0});

db.ciudades.count();


db.ciudades.find({$or: [{ciudad:/^B/} , {ciudad:/d$/}]} ,{ciudad:1,_id:0});

db.ciudades.updateOne({ciudad: 'Madrid'}, { $set: { "habitantes" : 20000 } });

db.ciudades.updateMany({ciudad:/a$/},{ $set: { "alcalde" : "" } });

db.ciudades.updateMany({ciudad:/^M/},{ $set: { "alcalde" : "Alejandro" } });

db.ciudades.insertOne(
    { 
        "ciudad" : "Bilbao", 
        "habitantes" : 5000 
});


db.ciudades.findOneAndUpdate({ciudad: "Bilbao"},{ $set: {ciudad: "BILBO"}});

db.ciudades.findOneAndUpdate({ciudad: "Madrid"},{ $set: {ciudad: "Madriz"}});

db.ciudades.updateOne({ciudad: 'Madrid'}, { $inc: { "habitantes" : 1000000 } });

db.ciudades.updateOne({ciudad: 'Barcelona'}, { $set: { "capital" : "no" } });

db.ciudades.find({alcalde: ""},{ciudad:1, _id:0});

db.ciudades.updateMany({ciudad:/^BI/},{ $set: { "alcalde" : "Celia" } });